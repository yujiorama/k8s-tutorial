Run a Stateless Application Using a Deployment
====

[Run a Stateless Application Using a Deployment](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/)

* やること
  - nginx をデプロイする
  - `kubectl` で様子を確認する
  - デプロイしたアプリを更新する


[deployment.yml](./deployment.yml)

```
$ kubectl apply -f deployment.yml
deployment "nginx-deployment" created
```

```
$ kubectl describe deployment nginx-deployment
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Wed, 07 Mar 2018 19:14:55 +0900
Labels:                 app=nginx
Annotations:            deployment.kubernetes.io/revision=1
                        kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"apps/v1","kind":"Deployment","metadata":{"annotations":{},"name":"nginx-deployment","namespace":"default"},"spec":{"replicas":2,"selecto...
Selector:               app=nginx
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.12.2
    Port:         80/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-9898d9674 (2/2 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  59s   deployment-controller  Scaled up replica set nginx-deployment-9898d9674 to 2
```

```
$ kubectl get pods -l app=nginx
NAME                               READY     STATUS    RESTARTS   AGE
nginx-deployment-9898d9674-g4wp6   1/1       Running   0          2m
nginx-deployment-9898d9674-s7kvn   1/1       Running   0          2m
```

```
$ kubectl describe pod nginx-deployment-9898d9674-g4wp6
Name:           nginx-deployment-9898d9674-g4wp6
Namespace:      default
Node:           minikube/192.168.99.100
Start Time:     Wed, 07 Mar 2018 19:14:56 +0900
Labels:         app=nginx
                pod-template-hash=545485230
Annotations:    <none>
Status:         Running
IP:             192.168.1.4
Controlled By:  ReplicaSet/nginx-deployment-9898d9674
Containers:
  nginx:
    Container ID:   docker://515f5bb7bbc6b94f9a3d7791da0e535256506bb781c68ec49d0375d87c639e77
    Image:          nginx:1.12.2
    Image ID:       docker-pullable://nginx@sha256:813be5ee155f914d063e8ed39878267995d31d7046a9010001f0a9e8192412a5
    Port:           80/TCP
    State:          Running
      Started:      Wed, 07 Mar 2018 19:15:10 +0900
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-vd4jc (ro)
Conditions:
  Type           Status
  Initialized    True
  Ready          True
  PodScheduled   True
Volumes:
  default-token-vd4jc:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-vd4jc
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     <none>
Events:
  Type    Reason                 Age   From               Message
  ----    ------                 ----  ----               -------
  Normal  Scheduled              3m    default-scheduler  Successfully assigned nginx-deployment-9898d9674-g4wp6 to minikube
  Normal  SuccessfulMountVolume  3m    kubelet, minikube  MountVolume.SetUp succeeded for volume "default-token-vd4jc"
  Normal  Pulling                3m    kubelet, minikube  pulling image "nginx:1.12.2"
  Normal  Pulled                 3m    kubelet, minikube  Successfully pulled image "nginx:1.12.2"
  Normal  Created                3m    kubelet, minikube  Created container
  Normal  Started                3m    kubelet, minikube  Started container
```

```
$ docker ps | grep nginx
515f5bb7bbc6        nginx                                         "nginx -g 'daemon ..."   4 minutes ago       Up 4 minutes                            k8s_nginx_nginx-deployment-9898d9674-g4wp6_default_624eca0b-21f0-11e8-ba35-080027b2325d_0
8aa1a474b383        nginx                                         "nginx -g 'daemon ..."   4 minutes ago       Up 4 minutes                            k8s_nginx_nginx-deployment-9898d9674-s7kvn_default_624e1d43-21f0-11e8-ba35-080027b2325d_0
14e2c6605672        gcr.io/google_containers/pause-amd64:3.0      "/pause"                 4 minutes ago       Up 4 minutes                            k8s_POD_nginx-deployment-9898d9674-g4wp6_default_624eca0b-21f0-11e8-ba35-080027b2325d_0
b5846e7d8d5e        gcr.io/google_containers/pause-amd64:3.0      "/pause"                 4 minutes ago       Up 4 minutes                            k8s_POD_nginx-deployment-9898d9674-s7kvn_default_624e1d43-21f0-11e8-ba35-080027b2325d_0
```


[deployment-update.yml](./deployment-update.yml)

```
$ kubectl apply -f deployment-update.yml
deployment "nginx-deployment" configured
```

```
$ kubectl get pods -l app=nginx
NAME                                READY     STATUS    RESTARTS   AGE
nginx-deployment-7b4c5cfb98-nqcd9   1/1       Running   0          18s
nginx-deployment-7b4c5cfb98-vtgpr   1/1       Running   0          26s
```

```
$ kubectl describe deployment nginx-deployment
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Wed, 07 Mar 2018 19:14:55 +0900
Labels:                 app=nginx
Annotations:            deployment.kubernetes.io/revision=2
                        kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"apps/v1","kind":"Deployment","metadata":{"annotations":{},"name":"nginx-deployment","namespace":"default"},"spec":{"replicas":2,"selecto...
Selector:               app=nginx
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.13.9-alpine
    Port:         80/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-7b4c5cfb98 (2/2 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  12m   deployment-controller  Scaled up replica set nginx-deployment-9898d9674 to 2
  Normal  ScalingReplicaSet  1m    deployment-controller  Scaled up replica set nginx-deployment-7b4c5cfb98 to 1
  Normal  ScalingReplicaSet  1m    deployment-controller  Scaled down replica set nginx-deployment-9898d9674 to 1
  Normal  ScalingReplicaSet  1m    deployment-controller  Scaled up replica set nginx-deployment-7b4c5cfb98 to 2
  Normal  ScalingReplicaSet  1m    deployment-controller  Scaled down replica set nginx-deployment-9898d9674 to 0
```

[deployment-scale.yml](./deployment-scale.yml)

```
$ kubectl apply -f deployment-scale.yml
deployment "nginx-deployment" configured
```

```
$ kubectl get pods -l app=nginx
NAME                                READY     STATUS    RESTARTS   AGE
nginx-deployment-7b4c5cfb98-7qgp9   1/1       Running   0          5s
nginx-deployment-7b4c5cfb98-8vkrr   1/1       Running   0          5s
nginx-deployment-7b4c5cfb98-nqcd9   1/1       Running   0          2m
nginx-deployment-7b4c5cfb98-vtgpr   1/1       Running   0          3m
```

```
$ kubectl describe deployment nginx-deployment
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Wed, 07 Mar 2018 19:14:55 +0900
Labels:                 app=nginx
Annotations:            deployment.kubernetes.io/revision=2
                        kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"apps/v1","kind":"Deployment","metadata":{"annotations":{},"name":"nginx-deployment","namespace":"default"},"spec":{"replicas":4,"selecto...
Selector:               app=nginx
Replicas:               4 desired | 4 updated | 4 total | 4 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.13.9-alpine
    Port:         80/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Progressing    True    NewReplicaSetAvailable
  Available      True    MinimumReplicasAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-7b4c5cfb98 (4/4 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  14m   deployment-controller  Scaled up replica set nginx-deployment-9898d9674 to 2
  Normal  ScalingReplicaSet  3m    deployment-controller  Scaled up replica set nginx-deployment-7b4c5cfb98 to 1
  Normal  ScalingReplicaSet  3m    deployment-controller  Scaled down replica set nginx-deployment-9898d9674 to 1
  Normal  ScalingReplicaSet  3m    deployment-controller  Scaled up replica set nginx-deployment-7b4c5cfb98 to 2
  Normal  ScalingReplicaSet  3m    deployment-controller  Scaled down replica set nginx-deployment-9898d9674 to 0
  Normal  ScalingReplicaSet  54s   deployment-controller  Scaled up replica set nginx-deployment-7b4c5cfb98 to 4
```

```
$ kubectl delete deployment nginx-deployment
deployment "nginx-deployment" deleted
```

```
$ kubectl get pods -l app=nginx
No resources found.
```

```
$ kubectl describe deployment nginx-deployment
Error from server (NotFound): deployments.extensions "nginx-deployment" not found
```
