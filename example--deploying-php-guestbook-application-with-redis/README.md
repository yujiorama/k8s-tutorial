Example: Deploying PHP Guestbook application with Redis
====

[Example: Deploying PHP Guestbook application with Redis](https://kubernetes.io/docs/tutorials/stateless-application/guestbook/)

* やること
    - leader/follower な構成で Redis をデプロイ
    - guestbook app をデプロイ
    - guestbook app を公開
    - 後始末


[redis-master-deployment.yaml](./redis-master-deployment.yaml)

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl apply -f redis-master-deployment.yaml
deployment "redis-master" created

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods -l app=redis
NAME                            READY     STATUS              RESTARTS   AGE
redis-master-585798d8ff-rqtdp   0/1       ContainerCreating   0          12s

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe deployment redis-master
Name:                   redis-master
Namespace:              default
CreationTimestamp:      Wed, 07 Mar 2018 19:39:05 +0900
Labels:                 app=redis
                        role=master
                        tier=backend
Annotations:            deployment.kubernetes.io/revision=1
                        kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"apps/v1","kind":"Deployment","metadata":{"annotations":{},"name":"redis-master","namespace":"default"},"spec":{"replicas":1,"selector":{...
Selector:               app=redis,role=master,tier=backend
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=redis
           role=master
           tier=backend
  Containers:
   master:
    Image:  k8s.gcr.io/redis:e2e
    Port:   6379/TCP
    Requests:
      cpu:        100m
      memory:     100Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   redis-master-585798d8ff (1/1 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  1m    deployment-controller  Scaled up replica set redis-master-585798d8ff to 1
```

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods -l app=redis,role=master -o json | jq -r '.items[] | .metadata.name'
redis-master-585798d8ff-rqtdp

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl logs -f $(kubectl get pods -l app=redis,role=master -o json | jq -r '.items[] | .metadata.name')
                _._
           _.-``__ ''-._
      _.-``    `.  `_.  ''-._           Redis 2.8.19 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._
 (    '      ,       .-`  | `,    )     Running in stand alone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
 |    `-._   `._    /     _.-'    |     PID: 1
  `-._    `-._  `-./  _.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |           http://redis.io
  `-._    `-._`-.__.-'_.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |
  `-._    `-._`-.__.-'_.-'    _.-'
      `-._    `-.__.-'    _.-'
          `-._        _.-'
              `-.__.-'

[1] 07 Mar 10:39:49.347 # Server started, Redis version 2.8.19
[1] 07 Mar 10:39:49.347 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
[1] 07 Mar 10:39:49.347 * The server is now ready to accept connections on port 6379
```

[redis-master-service.yaml](./redis-master-service.yaml)

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl apply -f redis-master-service.yaml
service "redis-master" created

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get service
NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
kubernetes     ClusterIP   10.96.0.1       <none>        443/TCP    9d
redis-master   ClusterIP   10.111.54.202   <none>        6379/TCP   17s

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe service redis-master
Name:              redis-master
Namespace:         default
Labels:            app=redis
                   role=master
                   tier=backend
Annotations:       kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"redis","role":"master","tier":"backend"},"name":"redis-master","names...
Selector:          app=redis,role=master,tier=backend
Type:              ClusterIP
IP:                10.111.54.202
Port:              <unset>  6379/TCP
TargetPort:        6379/TCP
Endpoints:         192.168.1.3:6379
Session Affinity:  None
Events:            <none>
```

[redis-slave-deployment.yaml](./redis-slave-deployment.yaml)
```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl apply -f redis-slave-deployment.yaml
deployment "redis-slave" created

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods -l app=redis,role=slave
NAME                           READY     STATUS              RESTARTS   AGE
redis-slave-865486c9df-72rdh   0/1       ContainerCreating   0          9s
redis-slave-865486c9df-nqnmr   0/1       ContainerCreating   0          9s

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe deployment redis-slave
Name:                   redis-slave
Namespace:              default
CreationTimestamp:      Wed, 07 Mar 2018 19:48:28 +0900
Labels:                 app=redis
                        role=slave
                        tier=backend
Annotations:            deployment.kubernetes.io/revision=1
                        kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"apps/v1","kind":"Deployment","metadata":{"annotations":{},"name":"redis-slave","namespace":"default"},"spec":{"replicas":2,"selector":{"...
Selector:               app=redis,role=slave,tier=backend
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=redis
           role=slave
           tier=backend
  Containers:
   slave:
    Image:  gcr.io/google_samples/gb-redisslave:v1
    Port:   6379/TCP
    Requests:
      cpu:     100m
      memory:  100Mi
    Environment:
      GET_HOSTS_FROM:  dns
    Mounts:            <none>
  Volumes:             <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   redis-slave-865486c9df (2/2 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  20s   deployment-controller  Scaled up replica set redis-slave-865486c9df to 2
```

[redis-slave-service.yaml](./redis-slave-service.yaml)

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl apply -f redis-slave-service.yaml
service "redis-slave" created

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get service
NAME           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
kubernetes     ClusterIP   10.96.0.1       <none>        443/TCP    9d
redis-master   ClusterIP   10.111.54.202   <none>        6379/TCP   5m
redis-slave    ClusterIP   10.104.45.245   <none>        6379/TCP   5s

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe service redis-slave
Name:              redis-slave
Namespace:         default
Labels:            app=redis
                   role=slave
                   tier=backend
Annotations:       kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"redis","role":"slave","tier":"backend"},"name":"redis-slave","namespa...
Selector:          app=redis,role=slave,tier=backend
Type:              ClusterIP
IP:                10.104.45.245
Port:              <unset>  6379/TCP
TargetPort:        6379/TCP
Endpoints:         192.168.1.4:6379,192.168.1.6:6379
Session Affinity:  None
Events:            <none>
```

[frontend-deployment.yaml](./frontend-deployment.yaml)

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl apply -f frontend-deployment.yaml
deployment "frontend" created

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods -l app=guestbook,tier=frontend
NAME                       READY     STATUS    RESTARTS   AGE
frontend-67f65745c-cgfk8   1/1       Running   0          1m
frontend-67f65745c-h5sbn   1/1       Running   0          1m
frontend-67f65745c-vj44p   1/1       Running   0          1m

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe deployment frontend
Name:                   frontend
Namespace:              default
CreationTimestamp:      Wed, 07 Mar 2018 19:52:40 +0900
Labels:                 app=guestbook
                        tier=frontend
Annotations:            deployment.kubernetes.io/revision=1
                        kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"apps/v1","kind":"Deployment","metadata":{"annotations":{},"name":"frontend","namespace":"default"},"spec":{"replicas":3,"selector":{"mat...
Selector:               app=guestbook,tier=frontend
Replicas:               3 desired | 3 updated | 3 total | 0 available | 3 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=guestbook
           tier=frontend
  Containers:
   php-redis:
    Image:  gcr.io/google-samples/gb-frontend:v4
    Port:   80/TCP
    Requests:
      cpu:     100m
      memory:  100Mi
    Environment:
      GET_HOSTS_FROM:  dns
    Mounts:            <none>
  Volumes:             <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      False   MinimumReplicasUnavailable
  Progressing    True    ReplicaSetUpdated
OldReplicaSets:  <none>
NewReplicaSet:   frontend-67f65745c (3/3 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  31s   deployment-controller  Scaled up replica set frontend-67f65745c to 3
```

[frontend-service.yaml](./frontend-service.yaml)

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl apply -f frontend-service.yaml
service "frontend" created

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get service
NAME           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
frontend       NodePort    10.101.201.176   <none>        80:31777/TCP   6s
kubernetes     ClusterIP   10.96.0.1        <none>        443/TCP        9d
redis-master   ClusterIP   10.111.54.202    <none>        6379/TCP       12m
redis-slave    ClusterIP   10.104.45.245    <none>        6379/TCP       6m

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe service frontend
Name:                     frontend
Namespace:                default
Labels:                   app=guestbook
                          tier=frontend
Annotations:              kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"guestbook","tier":"frontend"},"name":"frontend","namespace":"default"...
Selector:                 app=guestbook,tier=frontend
Type:                     NodePort
IP:                       10.101.201.176
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  31777/TCP
Endpoints:                192.168.1.7:80,192.168.1.8:80,192.168.1.9:80
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ minikube service list
|-------------|----------------------|-----------------------------|
|  NAMESPACE  |         NAME         |             URL             |
|-------------|----------------------|-----------------------------|
| default     | frontend             | http://192.168.99.100:31777 |
| default     | kubernetes           | No node port                |
| default     | redis-master         | No node port                |
| default     | redis-slave          | No node port                |
| kube-system | kube-dns             | No node port                |
| kube-system | kubernetes-dashboard | http://192.168.99.100:30000 |
|-------------|----------------------|-----------------------------|

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ minikube service --url frontend
http://192.168.99.100:31777
```

![guestbook][./guestbook-screenshot.png]

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl scale deployment frontend --replicas=4
deployment "frontend" scaled

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods
NAME                            READY     STATUS    RESTARTS   AGE
frontend-67f65745c-cgfk8        1/1       Running   0          11m
frontend-67f65745c-gr82r        1/1       Running   0          4s
frontend-67f65745c-h5sbn        1/1       Running   0          11m
frontend-67f65745c-vj44p        1/1       Running   0          11m
redis-master-585798d8ff-rqtdp   1/1       Running   0          25m
redis-slave-865486c9df-72rdh    1/1       Running   0          16m
redis-slave-865486c9df-nqnmr    1/1       Running   0          16m

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl scale deployment frontend --replicas=2
deployment "frontend" scaled

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods
NAME                            READY     STATUS        RESTARTS   AGE
frontend-67f65745c-cgfk8        1/1       Running       0          13m
frontend-67f65745c-gr82r        0/1       Terminating   0          2m
frontend-67f65745c-vj44p        1/1       Running       0          13m
redis-master-585798d8ff-rqtdp   1/1       Running       0          27m
redis-slave-865486c9df-72rdh    1/1       Running       0          18m
redis-slave-865486c9df-nqnmr    1/1       Running       0          18m
```

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl delete deployment -l app=redis
deployment "redis-master" deleted
deployment "redis-slave" deleted

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods
NAME                           READY     STATUS        RESTARTS   AGE
frontend-67f65745c-cgfk8       1/1       Running       0          15m
frontend-67f65745c-vj44p       1/1       Running       0          15m
redis-slave-865486c9df-72rdh   0/1       Terminating   0          19m
redis-slave-865486c9df-nqnmr   0/1       Terminating   0          19m

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get service
NAME           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
frontend       NodePort    10.101.201.176   <none>        80:31777/TCP   10m
kubernetes     ClusterIP   10.96.0.1        <none>        443/TCP        9d
redis-master   ClusterIP   10.111.54.202    <none>        6379/TCP       23m
redis-slave    ClusterIP   10.104.45.245    <none>        6379/TCP       17m

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe service redis-master
Name:              redis-master
Namespace:         default
Labels:            app=redis
                   role=master
                   tier=backend
Annotations:       kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"redis","role":"master","tier":"backend"},"name":"redis-master","names...
Selector:          app=redis,role=master,tier=backend
Type:              ClusterIP
IP:                10.111.54.202
Port:              <unset>  6379/TCP
TargetPort:        6379/TCP
Endpoints:         <none>
Session Affinity:  None
Events:            <none>

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe service redis-slave
Name:              redis-slave
Namespace:         default
Labels:            app=redis
                   role=slave
                   tier=backend
Annotations:       kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"redis","role":"slave","tier":"backend"},"name":"redis-slave","namespa...
Selector:          app=redis,role=slave,tier=backend
Type:              ClusterIP
IP:                10.104.45.245
Port:              <unset>  6379/TCP
TargetPort:        6379/TCP
Endpoints:         <none>
Session Affinity:  None
Events:            <none>
```

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl delete service -l app=redis
service "redis-master" deleted
service "redis-slave" deleted

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe service redis-slave
Error from server (NotFound): services "redis-slave" not found

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get service
NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
frontend     NodePort    10.101.201.176   <none>        80:31777/TCP   11m
kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP        9d
```

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl delete deployment -l app=guestbook
deployment "frontend" deleted

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods
NAME                       READY     STATUS        RESTARTS   AGE
frontend-67f65745c-vj44p   0/1       Terminating   0          16m

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get service
NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
frontend     NodePort    10.101.201.176   <none>        80:31777/TCP   12m
kubernetes   ClusterIP   10.96.0.1        <none>        443/TCP        9d

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl describe service frontend
Name:                     frontend
Namespace:                default
Labels:                   app=guestbook
                          tier=frontend
Annotations:              kubectl.kubernetes.io/last-applied-configuration={"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"guestbook","tier":"frontend"},"name":"frontend","namespace":"default"...
Selector:                 app=guestbook,tier=frontend
Type:                     NodePort
IP:                       10.101.201.176
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  31777/TCP
Endpoints:                <none>
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

```
y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl delete service -l app=guestbook
service "frontend" deleted

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get service
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   9d

y.okazawa@LPC-2068 MINGW64 ~/work/k8s-tutorial/example--deploying-php-guestbook-application-with-redis (master)
$ kubectl get pods
No resources found.
```
